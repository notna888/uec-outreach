# README #

* UEC Exploring Engineering Software - Version 1.0

This repo was made by Anton Savill for the UEC Exploring Engineering event 2017
(This readme was written like a year later)


### What is this repository for? ###

* It's just a few folders with the skeleton of some introduction to programming projects
* They only ever have to touch the js file, get them to open the html in chrome or something and let them know they just have to refresh the page to see it change.

1. Start - basic message output, also how comments work
2. If - explain that the computer makes a decision, also introduction into output
3. Loop - do something, do it again, and again, and again. Mention that computers are very good at this
4. pong - tie it all together and show that you can make something pretty cool. (pretty much stole and tidied up the code from here, https://www.youtube.com/watch?v=KoWqdEACyLI if someone is really keen you can give them this link here https://www.udemy.com/code-your-first-game/)
5. Open it up to questions and with the spare time it's best to ask them to play around with it, some suggestions below


### How do I start this? ###

* You'll need to log into about 20 computers (well, that's what I had to do, it depends how many students there are)
* Should maybe see if you can get "test" account system thingy going, like when you do tests and stuff, I'm not sure how the IT guys handle all that.
* Putting all the files on the desktop would usually work, but sometimes not, so might be best to use My Documents.
* Need to copy the folder a bunch of times, so there's a number for every student. otherwise one person editing it changes others, I went around at the start and just counted up and let each student know.
* Get them to open the files with notepad++ I explained that they can use pretty much any text editor that they want, but for simplicity we were just using that one (npp is on every computer in the MCL and ECL)
* run through the basic spiel, "who's done programming before?" (lots of students had done scratch, so they knew the real basics, but it's worth covering it anyway.)
* Sometimes you'll get one or two who had done it properly, and they get a bit bored, I had one kid that was keen to walk around and help.



### Ideas for them to play around with ###

1. Change the size of the panels
2. Change the size of the ball
3. change colours around
4. change the speed of the AI
5. change the speed of the ball
6. if there's a couple who are really good, mention that there's a way to key the keyboard to control the other paddle (so multiplayer)
7. You'll be surprised at what they come up with themselves


### Feedback ###
This is sort of general feedback, but felt like it's worth mentioning, here's the full list of feedback:
https://docs.google.com/spreadsheets/d/1TPo7cc3kkiwGDVlWjowKAr6mQgzGkCaSzOd6FigoNKE/edit#gid=0

* felt like could be more informative on pay scale & jobs
* it would be nice to hear what a week in a uni student is like
* Interactive
* Games are cool
* I like creating stuff on the computer
* not show and tell - (I think they mean they prefered interactive ones)
* it broadened my spectrum regarding the outputs of technology
* interactive fun, good memes. the code had nice garry in it (because I forgot to replace the code from stock at one point, memes can't hurt I guess)
* interactive and challenging
* software made my day. (but they then said that mining was their favourite activity **shakes fist angerly**)
* software ws cool its interesting to learn how computers work
* personal reasons
* nice garry (I'll take it)
* games, codes, fun

You'll never be the most popular though, chem nearly always get that because they make icecream.