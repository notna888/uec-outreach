
function doThing(){
  var wordBoxValue = document.getElementById("inputBox").value;
  var numberBoxValue = document.getElementById("numberBox").value;

  //most things in programming starts at 0
  // the " " is to put a space between the number and whatever you wrote
  // "\n" adds a new line at the end
  // varible += 1 means take the varible and add one to it
    // without the plus that just means you just make varible equal to 1 constantly
  for (var i = 0; i < numberBoxValue; i++) {
    document.getElementById("result").innerHTML += i + ": " + wordBoxValue + "\n";
  }
}
