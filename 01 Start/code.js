
function doThing(){
  var message = "Hello";
  // Two slashes like this means it's a comment, it lets me leave messages behind to future me
  // or stop a piece of code from running, remove the two slashes on the next line
  //alert(message);
}
