function startGame(){
  p1y=p2y=40; //player 1 and 2's starting height
  pt=10; //paddle thickness
  ph=100; //paddle height
  bx=by=50; //balls starting location
  bd=6; //ball diameter
  xv=yv=4; //balls initial velocity
  score1=score2=0; //initial score
  ais=2; //AI speed

  c=document.getElementById("gc");
  cc=c.getContext('2d');

  setInterval(update,1000/30);

  c.addEventListener('mousemove',function(e){
    p1y = e.clientY-ph/2;
  });
  }

  function reset(){
    bx=c.width/2; //middle width wise
    by=c.height/2; //middle height wise
    xv=-xv;
    yv=3;
  }

  function update(){

    bx+=xv;
    by+=yv;

    if(by<0 && yv<0){ // if ball location is at top and it is heading upwards
      yv=-yv; //reverse direction
    }

    if(by>c.height && yv>0){ // if ball location is at  and it heading downwards
      yv=-yv; //reverse direction
    }


    if(bx<0){ //ball is at left hand side
      if(by>p1y && by<p1y+ph){  //if it is within the boundries of paddle 1
        xv=-xv; //hit it back
        dy=by-(p1y+ph/2);
        yv=dy*0.3;
      } else {
        score2++;
        reset();
      }
    }
    if(bx>c.width){ //ball is at left hand side
      if(by>p2y && by<p2y+ph){  //if it is within the boundries of paddle 2
        xv=-xv; //hit it back
        dy=by-(p2y+ph/2);
        yv=dy*0.3;
      } else {
        score1++; //YAY!
        reset();
      }
    }

    if(p2y+ph/2<by){
      p2y += ais;
    } else if ((p2y+ph/2>by)) {
      p2y -= ais;
    }


    cc.fillStyle='black';
    cc.fillRect(0,0,c.width, c.height); //gameboard

    cc.fillStyle='white';
    cc.fillRect(0,p1y, pt, ph); //player 1
    cc.fillRect(c.width-pt,p2y, pt, ph); //player 2
    cc.fillRect(bx-bd/2,by-bd/2,bd,bd);
    cc.fillText(score1,100,100); //player one's score
    cc.fillText(score2,c.width-100,100); //player two's score
  }
